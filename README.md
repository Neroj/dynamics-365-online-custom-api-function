**Power BI Query editor function for dynamics 365 online**

**How does it work?**
<br>
Unlike the build-in Dynamics connector in Power BI this function uses REST (instead of odata) to only retrieve the columns that you need.
This makes the script in most cases both faster as well as it allows you to return tables that natively exceed the calculated column limit of the api.

**How to use it in Power BI?**
1. In Power BI Dekstop open the query editor ("Transform data" from the home ribbon )
2. Click "New source" => "Blank Query"
3. Replace the pre existing code with the code provided in this repository
4. Replace the BaseUrl with your own dynamics api endpoint.
5. Click "Done"
6. Navigate to query1 (*rename optional)
7. Enter the name of the table you want to retrieve
8. Enter the columns you want to retrieve seperated by a comma <br>
    Example: InvoiceDate,ShipDate,OrderID
9. Click Invoke


**Questions?**
<br>
Joren@WeAreBi.com
<br>
http://WeAreBi.com